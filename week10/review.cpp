#include <vector>
#include <iostream>
#include <utility> // for move

using namespace std;


class Potato {
  public:
    Potato(int size) : size(size) { t = new int[size];}
    Potato(const Potato& p) { this->size = p.size; this->t = p.t;  } // deep
    Potato(const Potato& p, bool shallow) { this->size = p.size;   } // shallow
    Potato(Potato&& p) : size(p.size), t(p.t) { p.t = nullptr; } // move
    ~Potato() { if (t != nullptr) delete [] t; }
    friend ostream & operator << (ostream& output, const Potato& p); 

  private: 
    unsigned int size;
    int *t{nullptr};
};

ostream & operator << (ostream& output, const Potato& p) {
    if (p.size > 10) {
        output << "I'm a big potato!";
    } else {
        output << "I'm a small potato!";
    }
    return output;
}

int main() {
    
    vector<Potato> v;

    for (int i = 1; i < 17; i+=4) 

        v.push_back(Potato(i));

    //for (auto it = v.begin(); it != v.end(); it++)  {
    for (vector<Potato>::iterator it = v.begin(); it != v.end(); it++)  {
	cout << *it << endl;
    }
    v.clear();
}
