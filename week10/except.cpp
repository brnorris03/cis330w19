// File: except.cpp
#include <iostream>
using namespace std;

/* Given:  StopNum   The stopping number to be used.
   Task:   To print successive integers 1, 2, 3, ... StopNum - 1.
           Throws an integer exception when StopNum is reached.
   Return: Nothing.
*/
void PrintSequence(int StopNum) {
   int Num;

   Num = 1;
   while (true) {
      if (Num >= StopNum)
         throw Num;
      cout << Num << endl;
      Num++;
   }
}

int main(void) {
   try {
      PrintSequence(20);
   } catch(int ExNum) {
      cout << "Caught an exception with value: " << ExNum << endl;
   }

   return 0;
}
