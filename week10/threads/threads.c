// file: threads.c
#include<stdio.h>
#include<pthread.h>
#include<unistd.h> //sleep
 
 
void* foo (void* msg) {
    sleep(2);
    printf(" In foo: %s\n", (const char *) msg);
}

void* bar (void* msg) {
    printf(" In bar: %s\n", (const char *) msg);
}

void main()
{
    pthread_t t1,t2;
 
    pthread_create(&t1, NULL, foo, "Hello from Task 1");
    pthread_create(&t2, NULL, bar, "Hello from Task 2");
    sleep(1); // something happening for a while
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
}
