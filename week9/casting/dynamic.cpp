#include <iostream>

using namespace std;

class Felid {
public:
  virtual void meow() = 0;
};

class Cat : public Felid {
public:
  void meow() { std::cout << "Meowing like a regular cat! meow!\n"; }
};

class WildCat : public Cat {
public:
  void meow() { std::cout << "Meowing like a wild cat! MRREOWW!\n"; }
};

int main() {
  try { 
    WildCat *Crazy = new WildCat; // Derived class
    Cat *Fluffy = new Cat;  // Base class

    WildCat *catPtr;

    catPtr = dynamic_cast<WildCat *>(Crazy);
    if (catPtr == nullptr) cout << "Null ptr on first type cast.\n";
    else catPtr->meow();

    catPtr = dynamic_cast<WildCat *>(Fluffy);
    if (catPtr == nullptr) cout << "Null ptr on second type cast.\n";
    else catPtr->meow();

  } catch (exception &e) {
    cout << "Exception occurred: " << e.what() << endl;
  }

  return 0;
}
