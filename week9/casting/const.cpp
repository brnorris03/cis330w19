#include <iostream> 
using namespace std;

void foo(string *s) { cout << *s << endl;}

int main() {
  const string *str = new string("Some text");
  foo(str);   // fix with const_cast
  return 0;
}
