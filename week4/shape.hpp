// File: shape.hpp
#ifndef __SHAPE_HPP
#define __SHAPE_HPP

#include <string>

// This is (moderately) bad
using namespace std;

namespace geometry {
  
  class Shape {
  public:
    Shape();
    virtual double getArea() = 0;
  protected:
    int color;
    std::string name;
  };
  
  class Rectangle : protected Shape {
  public:
    Rectangle(double length, double width);
    virtual double getArea();
  protected:
    double length;
  private:
    double width;
  };
  
  class Square : public Rectangle {
  public:
    Square();
    Square(double size);
  };
  
  
};

#endif
