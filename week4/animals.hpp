// File: animals.hpp
#ifndef __ANIMALS_HPP
#define __ANIMALS_HPP

#include <string> 
namespace animals {
  class Animal {
  public: 
    Animal() {}
    void eat();
    void sleep();
    void drink();
    bool isCarnivorous() { return carnivorous; }
  protected:
    int legs;
    int arms;
    int age;
    bool carnivorous;
  };
  
  class Quadruped : public Animal {
  public: 
    Quadruped() : Animal() { legs = 4; }
    void walk();
    void run();
  };
  
  inline namespace predators {
    class Wolf : public Quadruped {
    public:
      Wolf() : Quadruped() { carnivorous = true; }
      void hunt();
    };
  }
  
  class Color {
  public: 
    Color() : name("white") {}  //default color is white
    Color(std::string color) : name(color) {}  
    std::string getName() { return name;}
    void setName(std::string);
  private:
    std::string name;
  };
  
  class Wool {
  public:
    Wool() {}
    Wool(Color c) : color(c){} 
    Color getColor() { return color; }
    void setColor(Color c) { this->color = c; }
  private:
    Color color;
  };
  
  inline namespace livestock {
    class Sheep : public Quadruped {
    public: 
      Sheep() : Quadruped() { carnivorous = false; }
      Sheep(std::string color) : Quadruped() { wool.setColor(Color(color)); carnivorous = false; }
      void growWool(Color c) { wool.setColor(c); }
      Color getColor() { return wool.getColor(); }
    private:
      Wool wool;
    };
  }
  
}
#endif
