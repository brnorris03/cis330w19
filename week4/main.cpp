// File: main.cpp
#include <iostream>
#include "shape.hpp"

using namespace std;


int main() {
  
  // Note that the using new to allocate an array requires a default (no-argument) constructor
  geometry::Square *s1 = new geometry::Square[10]; // array of 10 Square objects
  
  geometry::Square *s2 = new geometry::Square(10); // a single Square object with length 10
  cout << s2->getArea() << endl;
  delete []s1;
  delete s2;
  
  // Solve world hunger, but without using s
  return 0;
}
