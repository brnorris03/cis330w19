#include <utility> // for move
#include <string>  // for string

class Student {
    private:
	std::string first;
	std::string last;
	int id;

    public:
	// Constructor #1
	Student(const std::string& f, const std::string& l="", int i = 0) 
	    : first(f), last(l), id(i) {
		// Question? How many copy assignments for std::string?
	    }

	// Move constructor (still need all combinations!)
	// CM1
	Student(const std::string&& f, const std::string&& l="", int i = 0) 
	    : first(std::move(f)), last(std::move(l)), id(i) {
	    }

	// CM2
	Student(const std::string& f, const std::string&& l="", int i = 0) 
	    : first(f), last(std::move(l)), id(i) {
	    }

	// CM3
	Student(const std::string&& f, const std::string& l="", int i = 0) 
	    : first(std::move(f)), last(l), id(i) {
	    }

};

int main() {
    Student s1{"Joe","Fix",42};   // CM1: 2 mallocs (2cr + 2mv)

    std::string s = "Joe";
    Student s2{s, "Fix", 42};	// CM2: 2 mallocs (1cp + 1cr+ 1mv)

    Student s3{std::move(s), "Fix", 42}; // CM3: 1 malloc (1mv + 1cr + 1 mv)

    // Student s4("Nick"); // ambiguous!
    // Student s5 = "Nick";   // ERROR: two implicit user-defined conversions
}

