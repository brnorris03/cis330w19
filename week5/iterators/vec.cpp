// File: vec.cpp

#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> myvector (5);  // 5 default-constructed ints
  auto v2 = myvector;

  // Iterate forwards, initializing the vector
  std::cout << "forward:";
  int i=0;

  //std::vector<int>::iterator it = myvector.begin();
  for (auto it = myvector.begin(); it!= myvector.end(); ++it) {
    // it is a pointer to the current object
    *it = ++i;   
    std::cout << ' ' << (*it);
  }
  std::cout << std::endl;


  // Iterate backwards
  std::cout << "backward:";

  int sum = 0;
  //std::vector<int>::reverse_iterator rit = myvector.rbegin();
  std::vector<int>::iterator rit;
  for (rit = myvector.end(); rit!= myvector.begin(); ) {
    --rit;
    // rit is a pointer to the current object
    std::cout << ' ' << (*rit); 
  }

  for (int i = 0; i < myvector.size(); i++) 
    sum += myvector[i];
  std::cout << std::endl << "Sum = " << sum << std::endl;

  return 0;
}
