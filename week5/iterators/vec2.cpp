// Run this with valgrind --leak-check=full to see if all memory is being freed
#include <iostream>
#include <vector>

using namespace std;

int main() {
    vector<int> v(10,-1);
    int j = 0;
    for (auto i = v.begin(); i != v.end(); i++) {
        (*i) = j++;
    }
    cout << "\nsize = " << v.size() << endl;
    for (vector<int>::reverse_iterator i = v.rbegin(); i != v.rend(); i++) {
        cout << (*i);
        v.pop_back();
    }

    cout << "\nsize = " << v.size() << endl;
    return 0;
}

