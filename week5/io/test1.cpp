// File: test1.cpp

#include <iostream>
using  namespace std;

void increment(int &x, int v) {
    x += v;
}

int main() {
    int x = 4;
    increment(x,5);
    cout << x << endl;
}
