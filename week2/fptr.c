#include <float.h> // for DBL_MAX
#include <stdlib.h>
#include <stdio.h>

#define NELS 3

// Note that you have to typedef List before 
// defining it because struct List uses it in fptr
typedef struct List List;

struct List {
    int numEls;
    double *elements; 
    double (*fptr) (List *self);
};

double computeAverage(List *self) {
    double avg = 0.0;
    for (int i = 0; i < self->numEls; i++) {
	avg += self->elements[i];
    }
    return avg / self->numEls;
}

double computeMin(List *self) {
    double min = DBL_MAX;
    for (int i = 0; i < self->numEls; i++) {
	if (min > self->elements[i]) {
	    min = self->elements[i];
	}
    }
    return min;
}

int main() {
  List list;

  // Initialize the list 
  list.numEls = NELS;
  
  // Using . instead of -> because list is not a pointer
  list.elements = (double *) malloc(NELS * sizeof(double));
  for (int i = 0; i < list.numEls; i++) {
      list.elements[i] = (double) i + 1.0;
  }

  // Compute average
  list.fptr = &computeAverage;
  printf("Average: %f\n", list.fptr(&list));

  // Compute minimum
  list.fptr = &computeMin;
  printf("Miniumum: %f\n", list.fptr(&list));
}
