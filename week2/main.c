#include <stdio.h>  // for printf
#include <stdlib.h> // for malloc
#include <string.h> // for strcpy and strlen

#include "puppy.h"

int main() {
    Name  puppy;
    int len = strlen("Archie");

    init("Archie", &puppy);
    // Do something interesting with puppy
    printf("Say hi to your new puppy -- %s!\n", puppy);
    release(&puppy); // sorry, puppy
}
