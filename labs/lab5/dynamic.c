typedef {
    char stuff[1000];
} Clock;


int main() {
  Clock **clocks;

  // 2 x 3 array of clocks, clocks[1][2]
  clocks = (Clock **) malloc( 2 * sizeof(Clock *));
  for (int i = 0; i < 2; i++)  {
    clocks[i] = (Clock *) malloc( 3 * sizeof(Clock));
    for (int j = 0; j < 3; j++) strcpy(clocks[i][j].stuff, "NO DATA");
    free(clocks[i]);
  }
  free(clocks);
}
  
