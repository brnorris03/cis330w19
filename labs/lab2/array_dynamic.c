#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#define ROW 3
#define COL 4

bool arrayEqual(int **a, int **b, int m, int n) {
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if (a[i][j] != b[i][j]) {
                return false;
	        }
	    }
    }
    return true;
}

void print_arr(int **arr, const char* indent) {
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            printf("%s%d ", indent,arr[i][j]);
        }
        printf("\n");
    }
}

int main(int argc, char *argv[]){

    int **x = NULL,  **y=NULL;
    srand(time(NULL));

    // Initialize to some random values
    x = (int **) malloc(ROW * sizeof(int*));
    y = (int **) malloc(ROW * sizeof(int*));
    for (int i = 0; i < ROW; i++) {
        x[i] = (int *) malloc (COL * sizeof(int*));
        y[i] = (int *) malloc (COL * sizeof(int*));
        for (int j = 0; j < COL; j++) {
            x[i][j] = rand() % 11;
	        y[i][j] = rand() % 11;
	    }
    }

    printf("\nFirst matrix x = \n"); 
    print_arr(x,"\t");
    printf("\nSecond matrix y = \n");
    print_arr(y,"\t");

    printf("\nIs x=y? %s\n", arrayEqual(x,y,ROW,COL) ? "Yes" : "No");
    printf("\nIs x=x? %s\n", arrayEqual(x,x,ROW,COL) ? "Yes" : "No");

    // Free memory
    for (int i = 0; i < ROW; i++) {
        free(x[i]);
        free(y[i]);
    }
    free(x);
    free(y);

    return 0;
}
