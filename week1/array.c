#include <stdlib.h>
#include <stdio.h>

void foo(int ***x) {

    *x = (int **)malloc(2 * sizeof(int*));
    for (int i = 0; i < 2; i++)  // BAD
        (*x)[i] = (int *) malloc ( 3 * sizeof(int) );

    for (int i = 0; i < 2; i++)  { // GOOD
        for (int j = 0; j < 3; j++) {
            (*x)[i][j] = j;
        }
    }

    for (int i = 0; i < 2; i++)  { // GOOD
        for (int j = 0; j < 3; j++) {
            printf("%d ", (*x)[i][j]);
        }
    }

    for (int i = 0; i < 2; i++)  // BAD
        free((*x)[i]);
    free(*x);
}

int main() {
    int **x = NULL;
    foo(&x);
    
}
